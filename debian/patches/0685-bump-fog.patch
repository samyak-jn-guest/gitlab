From 4193e3d47543abfd533f5bc73753a2d31ab9eb22 Mon Sep 17 00:00:00 2001
From: Stan Hu <stanhu@gmail.com>
Date: Wed, 23 Jan 2019 22:17:31 -0800
Subject: [PATCH 1/6] Bump fog-aws to 3.3.0 and associated dependencies

Fixes issue with AWS V4 signatures not working with Ceph S3:
https://github.com/fog/fog-aws/issues/462
---
 Gemfile                                       | 12 ++---
 Gemfile.lock                                  | 47 ++++++++++---------
 .../concerns/send_file_upload_spec.rb         |  2 +-
 3 files changed, 31 insertions(+), 30 deletions(-)

--- a/Gemfile
+++ b/Gemfile
@@ -95,13 +95,13 @@
 gem 'mini_magick'
 
 # for backups
-gem 'fog-aws', '~> 2.0', '>= 2.0.1'
-gem 'fog-core', '~> 1.44'
-gem 'fog-google', '~> 1.7', '>= 1.7.1'
-gem 'fog-local', '~> 0.3'
-gem 'fog-openstack', '~> 0.1'
+gem 'fog-aws', '~> 3.3'
+gem 'fog-core', '~> 2.1'
+gem 'fog-google', '~> 1.8'
+gem 'fog-local', '~> 0.6'
+gem 'fog-openstack', '~> 1.0'
 gem 'fog-rackspace', '~> 0.1.1'
-gem 'fog-aliyun', '~> 0.2.0'
+gem 'fog-aliyun', '~> 0.3'
 
 # for Google storage
 gem 'google-api-client', '~> 0.23'
--- a/Gemfile.lock
+++ b/Gemfile.lock
@@ -217,32 +217,33 @@
     flowdock (0.7.1)
       httparty (~> 0.7)
       multi_json
-    fog-aliyun (0.2.0)
-      fog-core (~> 1.27)
-      fog-json (~> 1.0)
+    fog-aliyun (0.3.3)
+      fog-core
+      fog-json
       ipaddress (~> 0.8)
       xml-simple (~> 1.1)
-    fog-aws (2.0.1)
-      fog-core (~> 1.38)
-      fog-json (~> 1.0)
+    fog-aws (3.3.0)
+      fog-core (~> 2.1)
+      fog-json (~> 1.1)
       fog-xml (~> 0.1)
       ipaddress (~> 0.8)
-    fog-core (1.45.0)
+    fog-core (2.1.0)
       builder
       excon (~> 0.58)
       formatador (~> 0.2)
-    fog-google (1.7.1)
-      fog-core
-      fog-json
-      fog-xml
+      mime-types
+    fog-google (1.8.2)
+      fog-core (<= 2.1.0)
+      fog-json (~> 1.2)
+      fog-xml (~> 0.1.0)
       google-api-client (~> 0.23.0)
-    fog-json (1.0.2)
-      fog-core (~> 1.0)
+    fog-json (1.2.0)
+      fog-core
       multi_json (~> 1.10)
-    fog-local (0.3.1)
-      fog-core (~> 1.27)
-    fog-openstack (0.1.21)
-      fog-core (>= 1.40)
+    fog-local (0.6.0)
+      fog-core (>= 1.27, < 3.0)
+    fog-openstack (1.0.8)
+      fog-core (~> 2.1)
       fog-json (>= 1.0)
       ipaddress (>= 0.8)
     fog-rackspace (0.1.1)
@@ -1004,12 +1005,12 @@
   flipper-active_record (~> 0.13.0)
   flipper-active_support_cache_store (~> 0.13.0)
   flowdock (~> 0.7)
-  fog-aliyun (~> 0.2.0)
-  fog-aws (~> 2.0.1)
-  fog-core (~> 1.44)
-  fog-google (~> 1.7.1)
-  fog-local (~> 0.3)
-  fog-openstack (~> 0.1)
+  fog-aliyun (~> 0.3)
+  fog-aws (~> 3.3)
+  fog-core (= 2.1.0)
+  fog-google (~> 1.8)
+  fog-local (~> 0.6)
+  fog-openstack (~> 1.0)
   fog-rackspace (~> 0.1.1)
   font-awesome-rails (~> 4.7)
   foreman (~> 0.84.0)
--- a/spec/controllers/concerns/send_file_upload_spec.rb
+++ b/spec/controllers/concerns/send_file_upload_spec.rb
@@ -112,7 +112,7 @@
 
         it 'sends a file with a custom type' do
           headers = double
-          expected_headers = %r(response-content-disposition=attachment%3B%20filename%3D%22test.js%22%3B%20filename%2A%3DUTF-8%27%27test.js&response-content-type=application/ecmascript)
+          expected_headers = /response-content-disposition=attachment%3B%20filename%3D%22test.js%22%3B%20filename%2A%3DUTF-8%27%27test.js&response-content-type=application%2Fecmascript/
           expect(Gitlab::Workhorse).to receive(:send_url).with(expected_headers).and_call_original
           expect(headers).to receive(:store).with(Gitlab::Workhorse::SEND_DATA_HEADER, /^send-url:/)
 
--- /dev/null
+++ b/config/initializers/fog_core_patch.rb
@@ -0,0 +1,52 @@
+# frozen_string_literal: true
+#
+# fog-core v2 changed the namespace format:
+#
+# Old: Fog::<service>::<provider> (e.g. Fog::Storage::AWS).
+# New: Fog::<provider>::<service> (e.g. Fog::AWS::Storage)
+#
+# To preserve backwards compatibility, fog-core v2.1.0 tries to load the
+# old schema first, but falls back to the older version if that
+# fails. This creates misleading warnings with fog-aws. See
+# https://github.com/fog/fog-aws/issues/504#issuecomment-468067991 for
+# more details.
+#
+# fog-core v2.1.2 reverses the load order
+# (https://github.com/fog/fog-core/pull/229), which works for fog-aws
+# but causes a stream of deprecation warnings for fog-google.
+# fog-google locked the dependency on fog-core v2.1.0 as a result
+# (https://github.com/fog/fog-google/issues/421) until the new namespace
+# is supported.
+#
+# Since we currently have some Fog gems that have not been updated, this
+# monkey patch makes a smarter decision about which namespace to try
+# first. This squelches a significant number of warning messages.
+#
+# Since this patch is mostly cosmetic, it can be removed safely at any
+# time, but it's probably best to wait until the following issues are
+# closed:
+#
+# fog-google: https://github.com/fog/fog-google/issues/421
+# fog-rackspace: https://github.com/fog/fog-rackspace/issues/29
+# fog-aliyun: https://github.com/fog/fog-aliyun/issues/23
+module Fog
+  module ServicesMixin
+    # Gems that have not yet updated with the new fog-core namespace
+    LEGACY_FOG_PROVIDERS = %w(google rackspace aliyun).freeze
+
+    def service_provider_constant(service_name, provider_name)
+      args = service_provider_search_args(service_name, provider_name)
+      Fog.const_get(args.first).const_get(*const_get_args(args.second))
+    rescue NameError # Try to find the constant from in an alternate location
+      Fog.const_get(args.second).const_get(*const_get_args(args.first))
+    end
+
+    def service_provider_search_args(service_name, provider_name)
+      if LEGACY_FOG_PROVIDERS.include?(provider_name.downcase)
+        [service_name, provider_name]
+      else
+        [provider_name, service_name]
+      end
+    end
+  end
+end
--- /dev/null
+++ b/changelogs/unreleased/sh-bump-fog-gem.yml
@@ -0,0 +1,5 @@
+---
+title: Fixes issue with AWS V4 signatures not working with some S3 providers
+merge_request: 21788
+author:
+type: fixed
